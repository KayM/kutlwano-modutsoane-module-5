import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:module5_firebase/membership_list.dart';

class RegistrationForm extends StatefulWidget {
  const RegistrationForm({Key? key, required String title}) : super(key: key);

  @override
  RegistrationFormState createState() {
    return RegistrationFormState();
  }
}

class RegistrationFormState extends State<RegistrationForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController dateOfBirthController = TextEditingController();

  Future registrationForm() {
    final name = nameController.text;
    final phone = phoneController.text;
    final dateOfBirth = dateOfBirthController.text;

    final ref = FirebaseFirestore.instance.collection('membership').doc();

    return ref
        .set({
          'Name': name,
          'Phone': phone,
          'DoB': dateOfBirth,
          'Doc ID': ref.id
        })
        .then(((value) => log('Membership added!')))
        .catchError((onError) => log(onError));
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextFormField(
                controller: nameController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.person),
                  hintText: 'Enter your name',
                  labelText: 'Name',
                ),
              ),
              TextFormField(
                controller: phoneController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.phone),
                  hintText: 'Enter a phone number',
                  labelText: 'Phone',
                ),
              ),
              TextFormField(
                controller: dateOfBirthController,
                decoration: const InputDecoration(
                  icon: Icon(Icons.calendar_today),
                  hintText: 'Enter your date of birth',
                  labelText: 'Dob',
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 150.0, top: 40.0),
                // ignore: deprecated_member_use
                child: Center(
                  child: ElevatedButton(
                    onPressed: () {
                      registrationForm();
                      {
                        nameController.text = '';
                        phoneController.text = '';
                        dateOfBirthController.text = '';
                      }
                    },
                    child: const Text('Register'),
                  ),
                ),
              ),
            ],
          ),
          const MembershipList()
        ]));
  }
}
