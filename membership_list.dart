import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class MembershipList extends StatefulWidget {
  const MembershipList({Key? key}) : super(key: key);

  @override
  State<MembershipList> createState() => _MembershipListState();
}

class _MembershipListState extends State<MembershipList> {
  final Stream<QuerySnapshot> _membership =
      FirebaseFirestore.instance.collection('membership').snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController nameFieldController = TextEditingController();
    TextEditingController phoneFieldController = TextEditingController();
    TextEditingController dobFieldController = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection('membership')
          .doc(docId)
          .delete()
          .then((value) => print('deleted'));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection('membership');
      print(data);
      nameFieldController.text = data['Name'];
      phoneFieldController.text = data['Phone'];
      dobFieldController.text = data['DoB'];

      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: const Text('Update'),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextField(
                      controller: nameFieldController,
                    ),
                    TextField(
                      controller: phoneFieldController,
                    ),
                    TextField(
                      controller: dobFieldController,
                    ),
                    TextButton(
                        onPressed: () {
                          collection.doc(data['Doc ID']).update({
                            'Name': nameFieldController.text,
                            'Phone': phoneFieldController.text,
                            'DoB': dobFieldController.text,
                          });
                          Navigator.pop(context);
                        },
                        child: const Text('Update'))
                  ],
                ),
              ));
    }

    return StreamBuilder(
        stream: _membership,
        builder: (BuildContext context,
            AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
          if (snapshot.hasError) {
            return const Text('something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          }

          if (snapshot.hasData) {
            return Row(children: [
              Expanded(
                child: SizedBox(
                    height: (MediaQuery.of(context).size.height),
                    width: (MediaQuery.of(context).size.width),
                    child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot documentSnapshot) {
                      Map<String, dynamic> data =
                          documentSnapshot.data() as Map<String, dynamic>;
                      return Column(
                        children: [
                          Card(
                            child: Column(children: [
                              ListTile(
                                leading: Text(data['Name']),
                                title: Text(data['Phone']),
                                subtitle: Text(data['DoB']),
                              ),
                              ButtonTheme(
                                child: ButtonBar(
                                  children: [
                                    OutlinedButton.icon(
                                      onPressed: () {
                                        _update(data);
                                      },
                                      icon: const Icon(Icons.edit),
                                      label: const Text('Edit'),
                                    ),
                                    OutlinedButton.icon(
                                      onPressed: () {
                                        _delete(data['Doc ID']);
                                      },
                                      icon: const Icon(Icons.remove),
                                      label: const Text('Delete'),
                                    ),
                                  ],
                                ),
                              ),
                            ]),
                          ),
                        ],
                      );
                    }).toList())),
              ),
            ]);
          } else {
            return const Text('No data');
          }
        });
  }
}
