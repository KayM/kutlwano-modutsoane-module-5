import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:module5_firebase/registration.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyCQl94_6tJeNhI_5Ic95rJVkE2S6nruAiY",
          authDomain: "module5-e2823.firebaseapp.com",
          projectId: "module5-e2823",
          storageBucket: "module5-e2823.appspot.com",
          messagingSenderId: "174740731884",
          appId: "1:174740731884:web:e355e63766d2e8bbe172c9"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Membership Registation',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const MyHomePage(title: 'Membership Registation'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              RegistrationForm(title: 'Membership Registration')
            ],
          ),
        ),
      ),
    );
  }
}
